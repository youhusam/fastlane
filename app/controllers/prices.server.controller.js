//TODO: Handle -1 prices for latest price
'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    request = require('request'),
    cheerio = require('cheerio'),
    moment = require('moment-timezone'),
    Price = mongoose.model('Price');

// Global variable for saving last week's price list.
var lastWeek;

/**
 * Update lastWeek global variable to lastWeek's prices
 * if found or the closest to last week.
 */
exports.updateLastWeek = function(req, res){
  var lastWeekDate = moment().tz('Asia/Jerusalem')
      .subtract(1, 'week').startOf('day').toDate();
  var query = {created: {$gte: lastWeekDate}};
  Price.find(query).sort({'created' : 1}).limit(1).exec(function (err, price) {
    if (err) return err;
    lastWeek = price;
    if(req === 'dayJob')
      return;
    else
      res.redirect('/api/lastWeekPrices');
  });
};

/**
 * Handle the price creation by getting data from fastlane website.
 * @param  {Object} price The Day object of the wanted price creation.
 */
var createPrice = function(price) {
  //connect to server
  request('https://www.fastlane.co.il/mobile.aspx',
    function (error, response, body) {
      var currentPrice = '-1';
      if (!error)
      {
        var htmlBody = cheerio.load(body);
        //get price text
        currentPrice = htmlBody('#lblPrice').text();
      }
      //convert string to float and push it to the document
      price.prices.push({
        'price': parseFloat(currentPrice),
        'created': moment().toDate()
      });
      price.save(function(err) {
        if(err) return err;
      });
  });
};

/**
 * Create or update the latest price document
 */
exports.create = function() {
  var dateNow = moment().tz('Asia/Jerusalem').startOf('day');
  Price.findOne({created: {$gte: dateNow}},
  function(err, price) {
    if(!price){
      price = new Price({day: dateNow.day(), created: dateNow});
    }
    createPrice(price);
  });
};

/**
 * Show the latest price
 */
exports.getLatest = function(req, res) {
  var dateNow = moment().tz('Asia/Jerusalem').startOf('day').toDate();
  Price.findOne({created: {$gte: dateNow}},function(err, price) {
    if(err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }
    if(!price){
      return res.status(404).send({
        message: 'Latest Entry not found'
      });
    }
    res.json([price.prices.pop()]);
  });
};

/**
 * List all prices
 */
exports.list = function(req, res) {
  Price.find().sort({'created' : -1}).exec(function(err, prices) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(prices);
    }
  });
};

/**
 * List prices since the begining of the current day
 */
exports.listDay = function(req, res) {
  var beginDate = moment().tz('Asia/Jerusalem').startOf('day').toDate();
  Price.find({created: {$gte: beginDate}}).sort({'created' : -1}).limit(1)
  .exec(function(err, priceList) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }
    if(!priceList || priceList.length === 0){
      return res.status(404).send({
        message: 'Cannot list today\'s prices'
      });
    }
    res.json(priceList);
  });
};

/**
 * List prices from a week
 */
exports.listWeek = function(req, res) {
  var beginDate = moment().tz('Asia/Jerusalem').day(-7).toDate();
  Price.getRange(beginDate, function(err, priceList) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }
    if(!priceList || priceList.length === 0){
      return res.status(404).send({
        message: 'Cannot list this week\'s prices'
      });
    }
    res.json(priceList);
  });
};

/**
 * List prices for a custom range
 */
exports.listCustom = function(req, res) {
  var beginDate = moment(req.query.beginDate).tz('Asia/Jerusalem')
                  .startOf('day').toDate();
  var endDate;
  if (req.query.endDate)
    endDate = moment(req.query.endDate).tz('Asia/Jerusalem').endOf('day')
              .toDate();
  else
    endDate = moment().tz('Asia/Jerusalem').toDate();
  Price.getRange(beginDate, endDate, function(err, priceList) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }
    if(!priceList || priceList.length === 0){
      return res.status(404).send({
        message: 'Could not find any prices in this range'
      });
    }
    res.json(priceList);
  });
};

/**
 * Return last week prices
 */
exports.getLastWeek = function(req, res){
  if(lastWeek)
    res.json(lastWeek);
  else
    res.redirect(307, '/api/updatelastweek');
};
