'use strict';

/**
 * Module Dependencies
 */
var CronJob = require('cron').CronJob,
    moment = require('moment-timezone'),
    prices = require('../../app/controllers/prices.server.controller');

// Get the timezone difference between Israel and the current server timezone
var jerusalemUTCDiff = moment.tz('Asia/Jerusalem').format('Z').split(':');
var currentUTCDiff = moment().format('Z').split(':');
var hoursDiff = parseInt(jerusalemUTCDiff[0]) - parseInt(currentUTCDiff[0]);
var minutesDiff = parseInt(jerusalemUTCDiff[1]) - parseInt(currentUTCDiff[1]);
var startOfHours = hoursDiff === 0 ? 0 : 24 - Math.abs(hoursDiff);
var startOfMinutes = Math.abs(minutesDiff);

/**
 * Create a CronJob that runs every 15 minutes and saves a new price
 * entry in the database.
 */
var job = new CronJob('0 */5 * * * *', function() {
  prices.create();
});

/**
 * Create a CronJob that runs everyday to update the week prices variable
 * Takes the start of day according to Jerusalem Time
 */
var dayJob = new CronJob('0 ' + startOfMinutes + startOfHours +  ' * * *', function () {
  prices.updateLastWeek('dayJob');
});

job.start();
dayJob.start();
