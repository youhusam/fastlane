'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    request = require('supertest'),
    app = require('../../server'),
    mongoose = require('mongoose'),
    Price = mongoose.model('Price');

var price;

/**
 * Prices routes test
 */

/*
* Route Disabled
describe('Price create test', function () {
  it('Should be able to create a price entry with no errors', function (done) {
   request(app).get('/api/createPrice')
     .end(function(req, res) {
       // Set assertion
       res.body.price.should.be.a.Number.and.be.within(5, 20);

       // Call the assertion callback
       done();
     });
 });
  afterEach(function(done) {
    Price.remove().exec(done);
  });
});
*/

describe('Price list routes tests', function () {
  before(function(done) {
    price = new Price({
      price: 7.85
    });
    done();
  });

  it('should be able to get a list of prices', function(done) {
    // Create new price model instance
    var priceObj = price;
    var date = new Date().toISOString();
    var date2 = new Date();
    date2.setHours(date2.getHours() - 1);
    date2 = date2.toISOString();
    var priceObj2 = new Price({
      price: 8,
      created: date2
    });
    priceObj.created = date;
    // Save the price
    priceObj.save(function() {
      // Request prices
      priceObj2.save(function() {

      request(app).post('/api/prices')
        .send({
          'beginDate': new Date(2010, 1, 1, 0 , 0 ,0)
        })
        .expect(200)
        .end(function(req, res) {
          // Set assertion
          res.body.should.be.an.Array.with.lengthOf(2);
          res.body[0].created.should.be.equal(date);

          // Call the assertion callback
          done();
        });
      });
    });
  });

  it('should be able to get the latest price', function(done) {
    // Create new price model instance
    var priceObj = new Price(price);

    // Save the price
    priceObj.save(function() {
      // Request prices
      request(app).get('/api/latestPrice')
        .end(function(req, res) {
          // Set assertion
          res.body[0].price.should.be.a.Number.and.be.equal(priceObj.price);

          // Call the assertion callback
          done();
        });
    });
  });

  afterEach(function(done) {
    Price.remove().exec(done);
  });

});

/*
describe('Price creator timer test', function () {

  it('Should be able to make a new price every 5 seconds', function (done) {
    this.timeout(16000);
    var counter = 0;
    var interval = setTimeout(function() {
      request(app).get('/api/prices')
        .end(function(req, res) {
          res.body.should.be.an.Array.with.lengthOf(3);
          done();
        });
    }, 15000);
  });

  afterEach(function(done) {
    Price.remove().exec(done);
  });
});
*/
