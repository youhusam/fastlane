'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
	mongoose = require('mongoose'),
	Price = mongoose.model('Price');

/**
 * Globals
 */
var price, price2;

/**
 * Unit tests
 */
describe('Price Model Unit Tests:', function() {
	before(function(done) {
		price = new Price({
			price: 7.85
		});
		price2 = new Price({
			price: '4'
		});
		done();
	});

	describe('Method Save', function() {
		it('should begin with no prices', function(done) {
			Price.find({}, function(err, prices) {
				prices.should.have.length(0);
				done();
			});
		});

		it('should be able to save without problems', function(done) {
			price.save(done);
		});

		it('should be able to show an error when try to save with a different object type',
		 function(done) {
				return price2.save(function(err) {
					should.exist(err);
					done();
			});
		});
	});

	describe('Method Get', function() {
		it('should be able to retun price as x.xx', function(done) {
			Price.find({price: 785}, function(err, prices) {
				should.not.exist(err);
				var price1 = prices[0].price;
				price1.should.be.equal(7.85);
				done();
			});
		});
	});

	after(function(done) {
		Price.remove().exec();
		done();
	});
});
