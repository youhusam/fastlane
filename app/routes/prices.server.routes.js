'use strict';

/**
 * Module Dependencies
 */
var prices = require('../../app/controllers/prices.server.controller');

module.exports = function(app) {
  app.route('/api/latestPrice').get(prices.getLatest);
  app.route('/api/prices').get(prices.listCustom);
  app.route('/api/todayPrices').get(prices.listDay);
  app.route('/api/weekPrices').get(prices.listWeek);
  app.route('/api/lastWeekPrices').get(prices.getLastWeek);
  app.route('/api/updatelastweek').get(prices.updateLastWeek);
  // app.route('/api/create').get(prices.create);

};
