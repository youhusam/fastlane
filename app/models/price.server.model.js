'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Price Getter for formatting the returned price, returns null if
 * price is -1.
 * @param  {number} num The price passed from the DB.
 * @return {number}     Float with 2 digits after the decimal point.
 */
var getPrice = function(num){
    return num !== -1? Math.round(num, 2)/100 : null;
};

/**
 * Price Setter that multipies the price before saving it in the db
 * if the type is not a number it'll pass undefined so that the save
 * method throws an error.
 * @param {number} num The price passed to the db.
 */
function setPrice(num) {
  return typeof num === 'number' ? num*100 : undefined;
}

/**
 * Schema definiton for the prices list
 * @type {Schema}
 */
var PricesSchema = new Schema({
    _id:false,
    id:false,
    price: {
      type: Number,
      get: getPrice,
      set: setPrice,
      required: 'Price cannot be blank',
      toObject : {getters: true},
      toJSON : {getters: true}
    },
    created: {
      type: Date,
      default: Date.now
    },
  },
  {
    toObject : {getters: true},
    toJSON : {getters: true},
    id: false
});

/**
 * Schema definiton for the price
 * @type {Schema}
 */
var DaySchema = new Schema({
  day:{
    type: Number,
    default: new Date().getDay()
  },
  created: {
    type: Date,
    default: Date.now
  },
  prices:[PricesSchema]
},
// Enable the getter property
{
  toObject : {getters: true},
  toJSON : {getters: true},
  id: false
});

/**
 * Returns queries from a date range.
 * @param  {Date Object}   beginDate  Range beginning.
 * @param  {Date Object}   endDate    Range end.
 * @param  {Function}      callback   Callback.
 * @return {[object]}                 Filtered data.
 */
DaySchema.statics.getRange = function(beginDate, endDate, callback) {
  if(typeof endDate === 'function'){
    callback = endDate;
    endDate = new Date();
  }
  endDate = endDate ? endDate : new Date();
  this.find({created: {$gt: beginDate, $lt: endDate}}).
      sort({'created' : -1}).exec(callback);
};

mongoose.model('Price', DaySchema);
