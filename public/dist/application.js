'use strict';

// Init the application configuration module for AngularJS application
var ApplicationConfiguration = (function() {
	// Init module configuration options
	var applicationModuleName = 'fastlane';
	var applicationModuleVendorDependencies = ['ngResource', 'ui.router', 'ui.utils',
	 'ngAria', 'ngAnimate', 'ngMaterial', 'highcharts-ng'];

	// Add a new vertical module
	var registerModule = function(moduleName, dependencies) {
		// Create angular module
		angular.module(moduleName, dependencies || []);

		// Add the module to the AngularJS configuration file
		angular.module(applicationModuleName).requires.push(moduleName);
	};

	return {
		applicationModuleName: applicationModuleName,
		applicationModuleVendorDependencies: applicationModuleVendorDependencies,
		registerModule: registerModule
	};
})();

'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config(['$locationProvider',
	function($locationProvider) {
		$locationProvider.hashPrefix('!');
	}
]);

//Then define the init function for starting up the application
angular.element(document).ready(function() {
	//Fixing facebook bug with redirect
	if (window.location.hash === '#_=_') window.location.hash = '#!';

	//Then init the app
	angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});

'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('core');
'use strict';

// Use application configuration module to register a new module
ApplicationConfiguration.registerModule('prices');

'use strict';

// Icons
angular.module('core').config(['$mdIconProvider',
  function( $mdIconProvider) {
    $mdIconProvider
      .icon('mainmenu', 'modules/core/img/icons/ic_menu_24px.svg')
      .icon('logo', 'modules/core/img/icons/ic_send_48px.svg');
  }
]);

'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {
		// Redirect to home view when route not found
		$urlRouterProvider.otherwise('/');

		// Home state routing
		$stateProvider.
		state('home', {
			title: 'Home',
			url: '/',
			templateUrl: 'modules/core/views/home.client.view.html'
		});
	}
]);

'use strict';

// Use the theming provider to change the colours
angular.module('core').config(['$mdThemingProvider', 
  function($mdThemingProvider) {
  $mdThemingProvider.theme('default')
    .primaryPalette('teal');
}]);

'use strict';

angular.module('core').controller('HeaderController', ['$scope', '$mdSidenav',
	function($scope, $mdSidenav, Menus) {
		$scope.toggleSidenav = function(menuId) {
		  $mdSidenav(menuId).toggle();
		};
	}
]);

'use strict';

angular.module('core').controller('HomeController', ['$scope',
	function($scope) {

	}
]);

'use strict';

angular.module('core').controller('MenuController', ['$scope', 'Menus', '$mdSidenav',
  function($scope, Menus, $mdSidenav) {
    $scope.menu = Menus.getMenu('left');
    $scope.toggleSidenav = function(menuId) {
      $mdSidenav(menuId).toggle();
    };
  }
]);

'use strict';

// Define a directive that doesn't add a new node in html
angular.module('core').directive('noInclude', ["$http", "$templateCache", "$compile", function ($http, $templateCache, $compile) {
  return {
    restrict: 'A',
    link: function (scope, element, attributes) {
      var templateUrl = scope.$eval(attributes.noInclude);
      $http.get(templateUrl, {cache: $templateCache}).success(
        function (tplContent) {
          element.replaceWith($compile(tplContent)(scope));
        }
      );
    }
  };
}]);

'use strict';

//Menu service used for managing  menus
angular.module('core').service('Menus', [

	function() {
		// Define a set of default roles
		this.defaultRoles = ['*'];

		// Define the menus object
		this.menus = {};

		// A private function for rendering decision
		var shouldRender = function() {

			return false;
		};

		// Validate menu existance
		this.validateMenuExistance = function(menuId) {
			if (menuId && menuId.length) {
				if (this.menus[menuId]) {
					return true;
				} else {
					throw new Error('Menu does not exists');
				}
			} else {
				throw new Error('MenuId was not provided');
			}

			return false;
		};

		// Get the menu object by menu id
		this.getMenu = function(menuId) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Return the menu object
			return this.menus[menuId];
		};

		// Add new menu object by menu id
		this.addMenu = function(menuId, isPublic, roles) {
			// Create the new menu
			this.menus[menuId] = {
				isPublic: isPublic || false,
				roles: roles || this.defaultRoles,
				items: [],
				shouldRender: shouldRender
			};

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeMenu = function(menuId) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Return the menu object
			delete this.menus[menuId];
		};

		// Add menu item object
		this.addMenuItem = function(menuId, menuItemTitle, menuItemURL, menuItemType, menuItemUIRoute, isPublic, roles, position) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Push new menu item
			this.menus[menuId].items.push({
				title: menuItemTitle,
				link: menuItemURL,
				menuItemType: menuItemType || 'item',
				menuItemClass: menuItemType,
				uiRoute: menuItemUIRoute || ('/' + menuItemURL),
				isPublic: ((isPublic === null || typeof isPublic === 'undefined') ? this.menus[menuId].isPublic : isPublic),
				roles: ((roles === null || typeof roles === 'undefined') ? this.menus[menuId].roles : roles),
				position: position || 0,
				items: [],
				shouldRender: shouldRender
			});

			// Return the menu object
			return this.menus[menuId];
		};

		// Add submenu item object
		this.addSubMenuItem = function(menuId, rootMenuItemURL, menuItemTitle, menuItemURL, menuItemUIRoute, isPublic, roles, position) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item
			for (var itemIndex in this.menus[menuId].items) {
				if (this.menus[menuId].items[itemIndex].link === rootMenuItemURL) {
					// Push new submenu item
					this.menus[menuId].items[itemIndex].items.push({
						title: menuItemTitle,
						link: menuItemURL,
						uiRoute: menuItemUIRoute || ('/' + menuItemURL),
						isPublic: ((isPublic === null || typeof isPublic === 'undefined') ? this.menus[menuId].items[itemIndex].isPublic : isPublic),
						roles: ((roles === null || typeof roles === 'undefined') ? this.menus[menuId].items[itemIndex].roles : roles),
						position: position || 0,
						shouldRender: shouldRender
					});
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeMenuItem = function(menuId, menuItemURL) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item to remove
			for (var itemIndex in this.menus[menuId].items) {
				if (this.menus[menuId].items[itemIndex].link === menuItemURL) {
					this.menus[menuId].items.splice(itemIndex, 1);
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeSubMenuItem = function(menuId, submenuItemURL) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item to remove
			for (var itemIndex in this.menus[menuId].items) {
				for (var subitemIndex in this.menus[menuId].items[itemIndex].items) {
					if (this.menus[menuId].items[itemIndex].items[subitemIndex].link === submenuItemURL) {
						this.menus[menuId].items[itemIndex].items.splice(subitemIndex, 1);
					}
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		//Adding the left menu
		this.addMenu('left');
	}
]);

'use strict';

// Give pages different titles
angular.module('core').run(['$rootScope', '$state',
  function($rootScope, $state) {
    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
      $rootScope.title = $state.current.title;
    });
}]);

'use strict';

// Configuring the Articles module
angular.module('prices').run(['Menus',
  function(Menus) {
    // Set top bar menu items
    Menus.addMenuItem('left', 'List Today\'s Prices', 'todayprices', 'item', '/todayprices');
    Menus.addMenuItem('left', 'List Week Prices', 'weekprices', 'item', '/weekprices');
    Menus.addMenuItem('left', 'Custom range Prices List', 'customprices', 'item', '/customprices');
  }
]);

'use strict';

// Settin up routes
angular.module('prices').config(['$stateProvider',
  function($stateProvider) {
    // Prices Routes
    $stateProvider.
      state('TodayPrices',{
        title: 'Today\'s Prices',
        url: '/todayprices',
        func: 'todayPrices',
        templateUrl: 'modules/prices/views/price-list.client.view.html'
      }).
      state('WeekPrices',{
        title: 'This Week\'s Prices',
        url: '/weekprices',
        func: 'weekPrices',
        templateUrl: 'modules/prices/views/price-list.client.view.html'
      }).
      state('CustomPrices',{
        title: 'Custom Range Prices',
        url: '/customprices',
        templateUrl: 'modules/prices/views/custom-price.client.view.html'
      });
  }
]);

'use strict';
/* global moment */
/* global Highcharts */

 angular.module('prices').controller('GraphController',['$scope', '$timeout',
  '$mdDialog', 'TodayPrices', 'LastWeekPrices', 'CustomPrices',
  function($scope, $timeout, $mdDialog, TodayPrices, LastWeekPrices,
           CustomPrices) {

    /**
     * Compare function for sorting.
     */
    function sortFunction(a, b) {
      if (a[0] === b[0]) {
          return 0;
      }
      else {
          return (a[0] < b[0]) ? -1 : 1;
      }
    }

    /**
     * Retrun an array of the time and prices from the data given from the server.
     */
    var fillData = function (data) {
      var pricesList = [],
      list = data.prices,
      today = moment().startOf('day').toArray();
      for (var id in list) {
        var time = moment(list[id].created).startOf('minute')
            // Set the date to today so that the graphs have the same date.
            .set({year: today[0], month: today[1], date: today[2]}).valueOf();
        var price = list[id].price;
        pricesList.push([time, price]);
      }
      return pricesList.sort(sortFunction);
    };

    /**
     * Get data from the server and load a modified version in the series
     */
    $scope.populateGraph = function() {
      var finished1 = false, finished2 = false;
      LastWeekPrices.query({}).$promise.then(function(data) {
        var pricesList = fillData(data[0]);
        $scope.graphConfig.series[0] = {
          name: moment(data[0].created).format('ddd, MMM D'),
          data: pricesList,
          zIndex: 1,
          color: '#808080',
          showInLegend: true,
          type: 'spline'
        };
        finished1 = true;
        if (finished1 && finished2) {
          $scope.dataLoaded = true;
        }
      },
      function(error) {
        $mdDialog.show(
          $mdDialog.alert()
           .parent(angular.element(document.body))
           .title('Error')
           .content(error.data.message)
           .ariaLabel('Error')
           .ok('Ok')
          );
      });
      TodayPrices.query({}).$promise.then(function(data) {
        var pricesList = fillData(data[0]);
        $scope.graphConfig.series[1] = {
          name: 'Today',
          data: pricesList,
          zIndex: 2,
          color: '#7CB5EC',
          showInLegend: true,
          type: 'areaspline'
        };
        finished2 = true;
        if (finished1 && finished2) {
          $scope.dataLoaded = true;
        }
      },
      function(error) {
        $mdDialog.show(
          $mdDialog.alert()
           .parent(angular.element(document.body))
           .title('Error')
           .content(error.data.message)
           .ariaLabel('Error')
           .ok('Ok')
          );
      });

    };

    /**
     * Change the date to compare with instead of last week.
     */
    $scope.getCustomPrices = function() {
      $scope.dataLoaded = false;
      var date = this.date;
      CustomPrices.query({
        'beginDate': date,
        'endDate': date
      }).$promise.then(function(data) {
        var pricesList = fillData(data[0]);
        $scope.graphConfig.series[0] = null;
        $scope.graphConfig.series[0]=({
          name: moment(data[0].created).format('ddd, MMM D'),
          data: pricesList,
          zIndex: 1,
          color: '#808080',
          showInLegend: true,
          type: 'spline'
        });
        $timeout(function () {
          // Reflow will resize the graph because it starts small
          $timeout(function () {
            // Zoom out the graph because it starts zoomed in
            $scope.graph.zoomOut();
          });
        });
        $scope.dataLoaded = true;
      },
      function(error) {
        $mdDialog.show(
          $mdDialog.alert()
           .parent(angular.element(document.body))
           .title('Error')
           .content('No prices data for ' +
              moment(date).format('DD-MM-YYYY') +
              ' Please choose another day')
           .ariaLabel('Error')
           .ok('Ok')
          );
          $scope.dataLoaded = true;
      });
    };

    $scope.graphConfig = {
      options: {
        tooltip:{
          xDateFormat: '%H:%M',
          valuePrefix: '\u20AA'
        },
        chart: {
          type: 'spline',
          zoomType: 'x'
        },
        rangeSelector: {
          enabled: false
        },
        navigator: {
          enabled: true
        },
        legend: {
          enabled: true
        }
      },
      useHighStocks: true,
      series: [],
      title: {
        text: ''
      },
      yAxis:{
        gridLineColor: '#E0F2F1',
        endOnTick: false,
        title:{
          text: 'Price'
        },
        opposite: false
      },
      loading: false,
      func: function(chart) {
        $scope.graph = chart;
        Highcharts.setOptions({
          global: {
            // Fix wrong date/time problem
            useUTC: false
          }
        });
        $scope.populateGraph();
        $timeout(function () {
          // Reflow will resize the graph because it starts small
          chart.reflow();
          $timeout(function () {
            // Zoom out the graph because it starts zoomed in
            chart.zoomOut();
          });
        });
      }
    };
  }
]);

'use strict';

angular.module('prices').controller('PricesController', ['$scope', '$state',
 '$mdDialog',
 'LatestPrice', 'TodayPrices', 'WeekPrices', 'CustomPrices',
  function($scope, $state, $mdDialog,
   LatestPrice, TodayPrices, WeekPrices, CustomPrices) {
    $scope.priceNow = function() {
      LatestPrice.query().$promise.then(function(data) {
        $scope.price = data;
        $scope.dataLoaded = true;
      },
      function(error) {
        displayError(error);
      });
    };

    var listPrices = function(priceList) {
      var prices = [];
      for (var day in priceList) {
        if (priceList[day].prices)
          for (var list in priceList[day].prices) {
            prices.push(priceList[day].prices[list]);
          }
      }
      $scope.prices = prices;
      $scope.headers = [{
          name: 'Price',
          field: 'price'
        },{
          name: 'created',
          field: 'created'
        }
      ];

      $scope.custom = {price: 'bold', created: 'grey'};
      $scope.sortable = ['price', 'created'];
      $scope.count = 50;
      $scope.dataLoaded = true;
    };

    var displayError = function(reason, ev) {
      $mdDialog.show(
        $mdDialog.alert()
         .parent(angular.element(document.body))
         .title('Error')
         .content(reason.data.message)
         .ariaLabel('Error')
         .ok('Ok')
         .targetEvent(ev)
        );
    };

    $scope.todayPrices = function() {
      TodayPrices.query().$promise.then(function(data) {
        listPrices(data);
      },
      function(error) {
        displayError(error);
      });
    };

    $scope.weekPrices = function() {
      WeekPrices.query().$promise.then(function(data) {
        listPrices(data);
      },
      function(error) {
        displayError(error);
      });
    };

    $scope.getCustomPrices = function() {
      $scope.showTable = true;
      $scope.dataLoaded = false;
      CustomPrices.query({
        'beginDate': this.beginDate,
        'endDate': this.endDate
      }).$promise.then(function(data) {
        listPrices(data);
      },
      function(error) {
        $scope.showTable = false;
        displayError(error);
      });
    };

    $scope.handleInit = function() {
      var funName = $state.current.func;
      return $scope[funName]();
    };
  }
]);

'use strict';

angular.module('prices').directive('mdTable', function () {
  return {
    restrict: 'E',
    scope: {
      headers: '=',
      content: '=',
      sortable: '=',
      filters: '=',
      customClass: '=customClass',
      count: '='
    },
    controller: ["$scope", "$filter", "$window", function ($scope,$filter,$window) {
      var orderBy = $filter('orderBy');
      $scope.tablePage = 0;
      var content = [];
      $scope.$watch('content', function(newValue, oldValue) {
        content = $scope.content;
      });

      $scope.nbOfPages = function () {
        if(content)
          return Math.ceil(content.length / $scope.count);
      };
      $scope.handleSort = function (field) {
        if ($scope.sortable.indexOf(field) > -1) { return true; } else { return false; }
      };
      $scope.order = function(predicate, reverse) {
        content = orderBy(content, predicate, reverse);
        $scope.predicate = predicate;
        if(content.length !== 0)
          $scope.content = content;
      };
      $scope.order($scope.sortable,false);
      $scope.getNumber = function (num) {
        return new Array(num);
      };
      $scope.goToPage = function (page) {
        $scope.tablePage = page;
      };
    }],
    templateUrl: '/modules/prices/views/md-table-template.client.view.html'
  };
});

'use strict';
/* global moment */

// REQUIRES:
// moment.js - https://github.com/moment/momentjs.com

// USAGE:
// {{ someDate | moment: [any moment function] : [param1] : [param2] : [param n]

// EXAMPLES:
// {{ someDate | moment: 'format': 'MMM DD, YYYY' }}
// {{ someDate | moment: 'fromNow' }}

// To call multiple moment functions, you can chain them.
// For example, this converts to UTC and then formats...
// {{ someDate | moment: 'utc' | moment: 'format': 'MMM DD, YYYY' }}

angular.module('prices').filter('moment', function () {
  return function (input, momentFn /*, param1, param2, etc... */) {
    var args = Array.prototype.slice.call(arguments, 2),
        momentObj = moment(input);
    return momentObj[momentFn].apply(momentObj, args);
  };
});

'use strict';

angular.module('prices').filter('startFrom',function (){
  return function (input,start) {
    if(input){
      start = +start;
      return input.slice(start);
    }
  };
});

'use strict';

//Prices service used for communicating with the prices REST endpoints
angular.module('prices').factory('LatestPrice', ['$resource',
  function($resource) {
    return $resource('/api/LatestPrice');
  }
]);

angular.module('prices').factory('TodayPrices', ['$resource',
  function($resource) {
    return $resource('/api/todayPrices');
  }
]);

angular.module('prices').factory('WeekPrices', ['$resource',
  function($resource) {
    return $resource('/api/weekPrices', {}, {isArray:true});
  }
]);

angular.module('prices').factory('CustomPrices', ['$resource',
  function($resource) {
    return $resource('/api/prices', {}, {isArray:true});
  }
]);

angular.module('prices').factory('LastWeekPrices', ['$resource',
  function($resource) {
    return $resource('/api/lastWeekPrices', {}, {isArray:true});
  }
]);
