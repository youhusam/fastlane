'use strict';
/* global moment */
/* global Highcharts */

 angular.module('prices').controller('GraphController',['$scope', '$timeout',
  '$mdDialog', 'TodayPrices', 'LastWeekPrices', 'CustomPrices',
  function($scope, $timeout, $mdDialog, TodayPrices, LastWeekPrices,
           CustomPrices) {

    /**
     * Compare function for sorting.
     */
    function sortFunction(a, b) {
      if (a[0] === b[0]) {
          return 0;
      }
      else {
          return (a[0] < b[0]) ? -1 : 1;
      }
    }

    /**
     * Retrun an array of the time and prices from the data given from the server.
     */
    var fillData = function (data) {
      var pricesList = [],
      list = data.prices,
      today = moment().startOf('day').toArray();
      for (var id in list) {
        var time = moment(list[id].created).startOf('minute')
            // Set the date to today so that the graphs have the same date.
            .set({year: today[0], month: today[1], date: today[2]}).valueOf();
        var price = list[id].price;
        pricesList.push([time, price]);
      }
      return pricesList.sort(sortFunction);
    };

    /**
     * Get data from the server and load a modified version in the series
     */
    $scope.populateGraph = function() {
      var finished1 = false, finished2 = false;
      LastWeekPrices.query({}).$promise.then(function(data) {
        var pricesList = fillData(data[0]);
        $scope.graphConfig.series[0] = {
          name: moment(data[0].created).format('ddd, MMM D'),
          data: pricesList,
          zIndex: 1,
          color: '#808080',
          showInLegend: true,
          type: 'spline'
        };
        finished1 = true;
        if (finished1 && finished2) {
          $scope.dataLoaded = true;
        }
      },
      function(error) {
        $mdDialog.show(
          $mdDialog.alert()
           .parent(angular.element(document.body))
           .title('Error')
           .content(error.data.message)
           .ariaLabel('Error')
           .ok('Ok')
          );
      });
      TodayPrices.query({}).$promise.then(function(data) {
        var pricesList = fillData(data[0]);
        $scope.graphConfig.series[1] = {
          name: 'Today',
          data: pricesList,
          zIndex: 2,
          color: '#7CB5EC',
          showInLegend: true,
          type: 'areaspline'
        };
        finished2 = true;
        if (finished1 && finished2) {
          $scope.dataLoaded = true;
        }
      },
      function(error) {
        $mdDialog.show(
          $mdDialog.alert()
           .parent(angular.element(document.body))
           .title('Error')
           .content(error.data.message)
           .ariaLabel('Error')
           .ok('Ok')
          );
      });

    };

    /**
     * Change the date to compare with instead of last week.
     */
    $scope.getCustomPrices = function() {
      $scope.dataLoaded = false;
      var date = this.date;
      CustomPrices.query({
        'beginDate': date,
        'endDate': date
      }).$promise.then(function(data) {
        var pricesList = fillData(data[0]);
        $scope.graphConfig.series[0] = null;
        $scope.graphConfig.series[0]=({
          name: moment(data[0].created).format('ddd, MMM D'),
          data: pricesList,
          zIndex: 1,
          color: '#808080',
          showInLegend: true,
          type: 'spline'
        });
        $timeout(function () {
          // Reflow will resize the graph because it starts small
          $timeout(function () {
            // Zoom out the graph because it starts zoomed in
            $scope.graph.zoomOut();
          });
        });
        $scope.dataLoaded = true;
      },
      function(error) {
        $mdDialog.show(
          $mdDialog.alert()
           .parent(angular.element(document.body))
           .title('Error')
           .content('No prices data for ' +
              moment(date).format('DD-MM-YYYY') +
              ' Please choose another day')
           .ariaLabel('Error')
           .ok('Ok')
          );
          $scope.dataLoaded = true;
      });
    };

    $scope.graphConfig = {
      options: {
        tooltip:{
          xDateFormat: '%H:%M',
          valuePrefix: '\u20AA'
        },
        chart: {
          type: 'spline',
          zoomType: 'x'
        },
        rangeSelector: {
          enabled: false
        },
        navigator: {
          enabled: true
        },
        legend: {
          enabled: true
        }
      },
      useHighStocks: true,
      series: [],
      title: {
        text: ''
      },
      yAxis:{
        gridLineColor: '#E0F2F1',
        endOnTick: false,
        title:{
          text: 'Price'
        },
        opposite: false
      },
      loading: false,
      func: function(chart) {
        $scope.graph = chart;
        Highcharts.setOptions({
          global: {
            // Fix wrong date/time problem
            useUTC: false
          }
        });
        $scope.populateGraph();
        $timeout(function () {
          // Reflow will resize the graph because it starts small
          chart.reflow();
          $timeout(function () {
            // Zoom out the graph because it starts zoomed in
            chart.zoomOut();
          });
        });
      }
    };
  }
]);
