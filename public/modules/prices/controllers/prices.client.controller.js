'use strict';

angular.module('prices').controller('PricesController', ['$scope', '$state',
 '$mdDialog',
 'LatestPrice', 'TodayPrices', 'WeekPrices', 'CustomPrices',
  function($scope, $state, $mdDialog,
   LatestPrice, TodayPrices, WeekPrices, CustomPrices) {
    $scope.priceNow = function() {
      LatestPrice.query().$promise.then(function(data) {
        $scope.price = data;
        $scope.dataLoaded = true;
      },
      function(error) {
        displayError(error);
      });
    };

    var listPrices = function(priceList) {
      var prices = [];
      for (var day in priceList) {
        if (priceList[day].prices)
          for (var list in priceList[day].prices) {
            prices.push(priceList[day].prices[list]);
          }
      }
      $scope.prices = prices;
      $scope.headers = [{
          name: 'Price',
          field: 'price'
        },{
          name: 'created',
          field: 'created'
        }
      ];

      $scope.custom = {price: 'bold', created: 'grey'};
      $scope.sortable = ['price', 'created'];
      $scope.count = 50;
      $scope.dataLoaded = true;
    };

    var displayError = function(reason, ev) {
      $mdDialog.show(
        $mdDialog.alert()
         .parent(angular.element(document.body))
         .title('Error')
         .content(reason.data.message)
         .ariaLabel('Error')
         .ok('Ok')
         .targetEvent(ev)
        );
    };

    $scope.todayPrices = function() {
      TodayPrices.query().$promise.then(function(data) {
        listPrices(data);
      },
      function(error) {
        displayError(error);
      });
    };

    $scope.weekPrices = function() {
      WeekPrices.query().$promise.then(function(data) {
        listPrices(data);
      },
      function(error) {
        displayError(error);
      });
    };

    $scope.getCustomPrices = function() {
      $scope.showTable = true;
      $scope.dataLoaded = false;
      CustomPrices.query({
        'beginDate': this.beginDate,
        'endDate': this.endDate
      }).$promise.then(function(data) {
        listPrices(data);
      },
      function(error) {
        $scope.showTable = false;
        displayError(error);
      });
    };

    $scope.handleInit = function() {
      var funName = $state.current.func;
      return $scope[funName]();
    };
  }
]);
