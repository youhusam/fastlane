'use strict';

//Prices service used for communicating with the prices REST endpoints
angular.module('prices').factory('LatestPrice', ['$resource',
  function($resource) {
    return $resource('/api/LatestPrice');
  }
]);

angular.module('prices').factory('TodayPrices', ['$resource',
  function($resource) {
    return $resource('/api/todayPrices');
  }
]);

angular.module('prices').factory('WeekPrices', ['$resource',
  function($resource) {
    return $resource('/api/weekPrices', {}, {isArray:true});
  }
]);

angular.module('prices').factory('CustomPrices', ['$resource',
  function($resource) {
    return $resource('/api/prices', {}, {isArray:true});
  }
]);

angular.module('prices').factory('LastWeekPrices', ['$resource',
  function($resource) {
    return $resource('/api/lastWeekPrices', {}, {isArray:true});
  }
]);
