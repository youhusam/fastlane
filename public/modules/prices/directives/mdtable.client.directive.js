'use strict';

angular.module('prices').directive('mdTable', function () {
  return {
    restrict: 'E',
    scope: {
      headers: '=',
      content: '=',
      sortable: '=',
      filters: '=',
      customClass: '=customClass',
      count: '='
    },
    controller: function ($scope,$filter,$window) {
      var orderBy = $filter('orderBy');
      $scope.tablePage = 0;
      var content = [];
      $scope.$watch('content', function(newValue, oldValue) {
        content = $scope.content;
      });

      $scope.nbOfPages = function () {
        if(content)
          return Math.ceil(content.length / $scope.count);
      };
      $scope.handleSort = function (field) {
        if ($scope.sortable.indexOf(field) > -1) { return true; } else { return false; }
      };
      $scope.order = function(predicate, reverse) {
        content = orderBy(content, predicate, reverse);
        $scope.predicate = predicate;
        if(content.length !== 0)
          $scope.content = content;
      };
      $scope.order($scope.sortable,false);
      $scope.getNumber = function (num) {
        return new Array(num);
      };
      $scope.goToPage = function (page) {
        $scope.tablePage = page;
      };
    },
    templateUrl: '/modules/prices/views/md-table-template.client.view.html'
  };
});
