'use strict';

// Configuring the Articles module
angular.module('prices').run(['Menus',
  function(Menus) {
    // Set top bar menu items
    Menus.addMenuItem('left', 'List Today\'s Prices', 'todayprices', 'item', '/todayprices');
    Menus.addMenuItem('left', 'List Week Prices', 'weekprices', 'item', '/weekprices');
    Menus.addMenuItem('left', 'Custom range Prices List', 'customprices', 'item', '/customprices');
  }
]);
