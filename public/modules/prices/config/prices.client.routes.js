'use strict';

// Settin up routes
angular.module('prices').config(['$stateProvider',
  function($stateProvider) {
    // Prices Routes
    $stateProvider.
      state('TodayPrices',{
        title: 'Today\'s Prices',
        url: '/todayprices',
        func: 'todayPrices',
        templateUrl: 'modules/prices/views/price-list.client.view.html'
      }).
      state('WeekPrices',{
        title: 'This Week\'s Prices',
        url: '/weekprices',
        func: 'weekPrices',
        templateUrl: 'modules/prices/views/price-list.client.view.html'
      }).
      state('CustomPrices',{
        title: 'Custom Range Prices',
        url: '/customprices',
        templateUrl: 'modules/prices/views/custom-price.client.view.html'
      });
  }
]);
