'use strict';

angular.module('prices').filter('startFrom',function (){
  return function (input,start) {
    if(input){
      start = +start;
      return input.slice(start);
    }
  };
});
