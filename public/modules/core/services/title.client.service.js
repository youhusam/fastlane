'use strict';

// Give pages different titles
angular.module('core').run(['$rootScope', '$state',
  function($rootScope, $state) {
    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
      $rootScope.title = $state.current.title;
    });
}]);
