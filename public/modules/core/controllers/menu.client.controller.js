'use strict';

angular.module('core').controller('MenuController', ['$scope', 'Menus', '$mdSidenav',
  function($scope, Menus, $mdSidenav) {
    $scope.menu = Menus.getMenu('left');
    $scope.toggleSidenav = function(menuId) {
      $mdSidenav(menuId).toggle();
    };
  }
]);
