'use strict';

angular.module('core').controller('HeaderController', ['$scope', '$mdSidenav',
	function($scope, $mdSidenav, Menus) {
		$scope.toggleSidenav = function(menuId) {
		  $mdSidenav(menuId).toggle();
		};
	}
]);
