'use strict';

// Use the theming provider to change the colours
angular.module('core').config(['$mdThemingProvider', 
  function($mdThemingProvider) {
  $mdThemingProvider.theme('default')
    .primaryPalette('teal');
}]);
