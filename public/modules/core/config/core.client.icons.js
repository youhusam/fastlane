'use strict';

// Icons
angular.module('core').config(['$mdIconProvider',
  function( $mdIconProvider) {
    $mdIconProvider
      .icon('mainmenu', 'modules/core/img/icons/ic_menu_24px.svg')
      .icon('logo', 'modules/core/img/icons/ic_send_48px.svg');
  }
]);
