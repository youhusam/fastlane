'use strict';

// Define a directive that doesn't add a new node in html
angular.module('core').directive('noInclude', function ($http, $templateCache, $compile) {
  return {
    restrict: 'A',
    link: function (scope, element, attributes) {
      var templateUrl = scope.$eval(attributes.noInclude);
      $http.get(templateUrl, {cache: $templateCache}).success(
        function (tplContent) {
          element.replaceWith($compile(tplContent)(scope));
        }
      );
    }
  };
});
